// Define a process named 'redmask'
process redmask {

    // Set a label for this process
    label 'midmem'

    // Set a tag for this process
    tag "redmask"

    // Define directories where output files will be published
    publishDir "${params.resultdir}/03_redmask", mode: 'copy', pattern: 'masked/*.msk'
    publishDir "${params.resultdir}/logs/redmask", mode: 'copy', pattern: 'redmask*.log'
    publishDir "${params.outdir}/00_pipeline_info/cmd", mode: 'copy', pattern: 'redmask*.cmd'

    // Define input parameter for this process
    input:
        // Path to the assembled contigs
        path(assembly_fa)

    // Define output files for this process
    output:
        // Output path for the masked sequences (*.msk)
        path("masked/*.msk"), emit: redmask_ch
        // Output path for redmask log files (redmask*.log)
        path("redmask*.log")
        // Output path for redmask command files (redmask*.cmd)
        path("redmask*.cmd")

    // Define the script to be executed for this process
    script:
    """
    redmask.sh $assembly_fa ${task.cpus} redmask.cmd >& redmask.log 2>&1
    """ 
}

