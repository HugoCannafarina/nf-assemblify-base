process braker3 {
    // This process is named 'braker3'

    // Labeling this process as requiring high memory
    label 'highmem'

    // Tagging this process with 'braker3'
    tag "braker3"

    // Publishing output GFF3 files to the specified directory
    publishDir "${params.resultdir}/05a_braker3", mode: 'copy', pattern: '*.gff3'
    // Publishing output amino acid files to the specified directory
    publishDir "${params.resultdir}/05a_braker3", mode: 'copy', pattern: '*.aa'
    // Publishing Braker3 logs to the specified directory
    publishDir "${params.resultdir}/logs/braker3", mode: 'copy', pattern: '*.log'
    // Publishing Braker3 logs to the specified directory (duplicate line, can be removed)
    publishDir "${params.resultdir}/logs/braker3", mode: 'copy', pattern: '*.log'
    // Publishing command files to the specified directory
    publishDir "${params.outdir}/00_pipeline_info/cmd", mode: 'copy', pattern: '*.cmd'

    // Defining input parameters
    input:
        path(assembly_fa)
        val(species_ch)
        path(samtools_bam_ch)
        path(prot_ch)

    // Defining output files
    output:
        path("*.gff3"), emit: braker3_gff3_ch
        path("*.aa"), emit: braker3_faa_ch
        path("*.log")
        path("*.cmd")

    // Executing the Braker3 script
    script:
    """
    braker3.sh $assembly_fa $species_ch $samtools_bam_ch $prot_ch ${task.cpus} braker3.cmd >& braker3.log 2>&1
    """
}


