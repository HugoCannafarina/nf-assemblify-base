process multiqcLOGS {
    // This process is named 'multiqcLOGS'

    // Labeling this process as requiring low memory
    label 'lowmem'

    // Tagging this process with 'multiQC_LOGS_hisat2'
    tag "multiQC_LOGS_hisat2"

    // Publishing MultiQC HTML reports to the specified directory
    publishDir "${params.resultdir}/04c_multiqc", mode: 'copy', pattern: '*.html'
    // Publishing MultiQC logs to the specified directory
    publishDir "${params.resultdir}/logs/multiqc_hisat2", mode: 'copy', pattern: 'multiqc*.log'
    // Publishing command files to the specified directory
    publishDir "${params.outdir}/00_pipeline_info/cmd", mode: 'copy', pattern: 'multiqc*.cmd'

    // Defining input parameters
    input:
        path(hisat2_logs_ch)

    // Defining output files
    output:
        path("*.html")
        path("multiqc*.log")
        path("multiqc*.cmd")

    // Executing the MultiQC script
    script:
    """
    multiqc.sh "." "multiqc_LOGS_report" multiqc.cmd >& multiqc.log 2>&1
    """
}


