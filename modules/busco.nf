// Define a process named 'busco'
process busco {

    // Set a label for this process
    label 'midmem'

    // Set a tag for this process
    tag "busco"

    // Define directories where output files will be published
    publishDir "${params.resultdir}/02b_busco", mode: 'copy', pattern: '*.busco'
    publishDir "${params.resultdir}/logs/busco", mode: 'copy', pattern: 'busco*.log'
    publishDir "${params.outdir}/00_pipeline_info/cmd", mode: 'copy', pattern: 'busco*.cmd'

    // Define input parameters for this process
    input:
        // Paths to the BUSCO database directory
        path(budsco_db_path)
        // Name of the BUSCO database
        val(busco_db_name)
        // Path to the assembly FASTA file
        path(assembly_fa)

    // Define output files for this process
    output:
        // Output path for BUSCO result files (*.busco)
        path("*.busco")
        // Output path for BUSCO log files (busco*.log)
        path("busco*.log")
        // Output path for BUSCO command files (busco*.cmd)
        path("busco*.cmd")

    // Define the script to be executed for this process
    script:
    """
    busco.sh ${task.cpus} ${budsco_db_path} ${busco_db_name} ${assembly_fa} busco.cmd >& busco.log 2>&1
    """ 
    // Execute the busco.sh script with the specified parameters
    // Redirect standard output and standard error to log files
}
