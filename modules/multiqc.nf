// Define a process named 'multiqc'
process multiqc {

    // Set a label for this process
    label 'lowmem'

    // Set a tag for this process
    tag "multiQC"

    // Define directories where output files will be published
    publishDir "${params.resultdir}/01b_multiqc", mode: 'copy', pattern: '*.html'
    publishDir "${params.resultdir}/logs/multiqc", mode: 'copy', pattern: 'multiqc*.log'
    publishDir "${params.outdir}/00_pipeline_info/cmd", mode: 'copy', pattern: 'multiqc*.cmd'

    // Define input parameter for this process
    input:
        // Path to the quality-controlled reads (output from previous process)
        path(qc_ch)

    // Define output files for this process
    output:
        // Output path for the MultiQC HTML report files (*.html)
        path("*.html")
        // Output path for MultiQC log files (multiqc*.log)
        path("multiqc*.log")
        // Output path for MultiQC command files (multiqc*.cmd)
        path("multiqc*.cmd")

    // Define the script to be executed for this process
    script:
    """
    multiqc.sh "." "multiqc_report" multiqc.cmd >& multiqc.log 2>&1
    """ 
    // Execute the multiqc.sh script with the specified parameters
    // Redirect standard output and standard error to log files
}


