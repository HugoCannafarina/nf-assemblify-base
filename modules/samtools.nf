process samtools {
    // This process is named 'samtools'

    // Labeling this process as requiring high memory
    label 'highmem'

    // Tagging this process with a tag that includes the base name of the input SAM file
    tag "samtools_${hisat2_sam_ch.baseName}"

    // Publishing BAM files to the specified directory
    publishDir "${params.resultdir}/04b_samtools", mode: 'copy', pattern: '*.bam'
    // Publishing BAI files to the specified directory
    publishDir "${params.resultdir}/04b_samtools", mode: 'copy', pattern: '*.bai'
    // Publishing Samtools logs to the specified directory
    publishDir "${params.resultdir}/logs/samtools", mode: 'copy', pattern: '*.log'
    // Publishing command files to the specified directory
    publishDir "${params.outdir}/00_pipeline_info/cmd", mode: 'copy', pattern: '*.cmd'

    // Defining input parameters, each input is a SAM file
    input:
        each(hisat2_sam_ch)

    // Defining output files
    output:
        path("*.bam"), emit: samtools_bam_ch
        path("*.bai"), emit: samtools_bai_ch
        path("*.log")
        path("*.cmd")
        
    // Executing the Samtools script
    script:
    """
    samtools.sh ${task.cpus} $hisat2_sam_ch  samtools_${hisat2_sam_ch.baseName}.cmd >& samtools_${hisat2_sam_ch.baseName}.log 2>&1
    """
}


