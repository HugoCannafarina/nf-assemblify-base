// Define a process named 'hifiasm'
process hifiasm {

    // Set a label for this process
    label 'midmem'

    // Set a tag for this process
    tag "hifiasm"

    // Define directories where output files will be published
    publishDir "${params.resultdir}/02a_hifiasm", mode: 'copy', pattern: '*.bp.p_ctg.fa'
    publishDir "${params.resultdir}/logs/hifiasm", mode: 'copy', pattern: 'hifi*.log'
    publishDir "${params.outdir}/00_pipeline_info/cmd", mode: 'copy', pattern: 'hifi*.cmd'

    // Define input parameter for this process
    input:
        // Path to the quality-controlled reads (output from previous process)
        path(qc_ch)

    // Define output files for this process
    output:
        // Output path for the assembled contigs (assembly_fa is emitted to make it available to downstream processes)
        path("*.bp.p_ctg.fa"), emit: assembly_fa
        // Output path for hifiasm log files (hifi*.log)
        path("hifi*.log")
        // Output path for hifiasm command files (hifi*.cmd)
        path("hifi*.cmd")

    // Define the script to be executed for this process
    script:
    """
    hifiasm.sh $qc_ch ${task.cpus} hifiasm.cmd >& hifiasm.log 2>&1
    """ 
    // Execute the hifiasm.sh script with the specified parameters
    // Redirect standard output and standard error to log files
}




