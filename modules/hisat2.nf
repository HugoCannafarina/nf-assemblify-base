process hisat2 {
    // This process is named 'hisat2'

    // Labeling this process as requiring high memory
    label 'highmem'

    // Tagging this process with 'hisat2'
    tag "hisat2"

    // Publishing index files to the specified directory
    publishDir "${params.resultdir}/04a_hisat2", mode: 'copy', pattern: '*.ht2'
    // Publishing SAM files to the specified directory
    publishDir "${params.resultdir}/04a_hisat2", mode: 'copy', pattern: '*.sam'
    // Publishing Hisat2 logs to the specified directory
    publishDir "${params.resultdir}/logs/hisat2", mode: 'copy', pattern: '*.log'
    // Publishing command files to the specified directory
    publishDir "${params.outdir}/00_pipeline_info/cmd", mode: 'copy', pattern: '*.cmd'

    // Defining input parameters
    input:
        path(redmask_ch)
        path(r1_illumina_ch)
        path(r2_illumina_ch)

    // Defining output files
    output:
        path("*.sam"), emit: hisat2_sam_ch
        path("*.ht2")
        path("*.log"), emit: hisat2_log_ch
        path("*.cmd")

    // Executing the Hisat2 script
    script:
    """
    hisat2.sh ${task.cpus} $redmask_ch $r1_illumina_ch $r2_illumina_ch hisat2.cmd >& hisat2.log 2>&1
    """
}
