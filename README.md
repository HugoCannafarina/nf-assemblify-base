#  Assemblify-base pipeline

This is a project associated to this [training material](https://bioinfo.gitlab-pages.ifremer.fr/teaching/fair-gaa/practical-case/).

## Dunaliella primolecta Genome Analysis Pipeline

This Nextflow pipeline is originally designed for the analysis of the Dunaliella primolecta genome using various bioinformatics tools including nanoplot, FastQC, MultiQC, BUSCO, hifiasm, and RedMask.


## Introduction

Dunaliella primolecta is a species of unicellular, halotolerant green algae known for its ability to produce high levels of beta-carotene. This pipeline aims to provide a comprehensive analysis of the D. primolecta genome using bioinformatics tools.

## Data description

The data entered into the pipeline should be of the format described in the following [document](https://practical-case-bioinfo-teaching-fair-gaa-92d332f8346e321adf6fde.gitlab-pages.ifremer.fr/#what-we-expect-from-you).

It contains the following variables : 

- Study Accession Number

- Sample Accession Number

- Run Accession Number

- Scientific Name

- Library Construction Protocol 

Following this strucuture, this pipeline is usable for all similar species. 

## Pipeline Overview

The pipeline consists of the following main steps:

1. **Data Preprocessing**: Quality assessment and trimming of raw sequencing data using FastQC.

2. **Genome Assembly**: De novo assembly of the D. primolecta genome using hifiasm.

3. **SAM Processing**: Conversion of SAM files to BAM format and sorting/indexing using SAMtools.

4. **MultiQC Processing**: Quality assessment and trimming of processed sequencing data using QC.

5. **Genome Assessment**: Evaluation of the assembled genome using BUSCO to assess completeness and accuracy.

6. **Repeat Masking**: Identification and masking of repetitive elements in the genome using RedMask.

7. **Structural Variant Calling**: Identification of structural variants using Braker3.



## Usage

### Requirements

- Nextflow (>= 23.04.0)

- Fastqc (>= 0.11.9)

- Multiqc (>= 1.14)

- hifiasm (>= 0.18.9)

- nanoplot (>= 1.28.4)

- busco (>= 5.5.0)

- redmask (>= 2018.09.10)

- hisat2 (>= 2.2.1)
