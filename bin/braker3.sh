#!/usr/bin/env bash
###############################################################################
##               	Annotation using braker3                                 ##
###############################################################################

args=("$@")
GENOME=${args[0]} 
SPECIES=${args[1]}
BAM=${args[2]} 
PROT=${args[3]} 
NCPUS=${args[4]}
LOGCMD=${args[5]}

# Command to execute
CMD="braker.pl --species=${SPECIES} --genome=${GENOME} --bam=${BAM} --prot_seq=${PROT} \
          --workingdir=braker3 --gff3 --threads ${NCPUS}"
       
# Keep command in log
echo $CMD > $LOGCMD

# Run Commands
eval $CMD
