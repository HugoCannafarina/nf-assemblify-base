#!/usr/bin/env bash
######################################################
##  Samtools for converting SAM to BAM and indexing ##                                                                    ##
######################################################

# var settings
args=("$@")
NCPUS=${args[0]}
SAM=${args[1]}
LOGCMD=${args[2]}

BAM=$(basename ${SAM%.*}).bam
CMD1="samtools sort -o $BAM -@ $NCPUS -O bam -T tmp $SAM"
CMD2="samtools index $BAM"

# Keep command in log
echo $CMD1 > $LOGCMD
echo $CMD2 >> $LOGCMD

# Run Command
eval ${CMD1}
eval ${CMD2}

