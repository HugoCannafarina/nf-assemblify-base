#!/usr/bin/env bash
###############################################################################                                                          
#####                 Create index and read maps using HISAT2             #####                                                                           
###############################################################################

# var settings
args=("$@")
NCPUS=${args[0]}
REDMASK=${args[1]}
R1=${args[2]}
R2=${args[3]}
LOGCMD=${args[4]}

CMD="hisat2-build -p $NCPUS $REDMASK ${REDMASK%.*} ; hisat2 -p $NCPUS --no-unal -q -x ${REDMASK%.*} -1 ${R1} -2 ${R2} --max-intronlen 20000 > hisat2_output.sam"
       
# Keep command in log
echo $CMD > $LOGCMD

# Run Commands
eval $CMD