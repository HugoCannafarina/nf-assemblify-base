#!/usr/bin/env bash
###############################################################################
##                                                                           ##
##               	Quality control of reads using nanoplot                  ##
##                                                                           ##
###############################################################################

# var settings
args=("$@")
fastq=${args[0]}
LOGCMD=${args[1]}

# Command to execute
CMD="NanoPlot --plots kde dot hex --N50 --title \"PacBio Hifi reads\" --fastq ${fastq} -o ."

# Keep command in log
echo ${CMD} > ${LOGCMD}

# Execute command
eval ${CMD}

